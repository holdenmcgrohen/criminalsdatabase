﻿namespace CriminalsDatabase.Web.Models
{
    public class MessageViewModel
    {
        public string Header { get; set; }

        public string Text { get; set; }
    }
}