﻿using CriminalsDatabase.Domain;
using System;
using System.Linq;
using System.Web.Mvc;

namespace CriminalsDatabase.Web.Models
{
    public class SearchViewModel
    {
        public SearchViewModel()
        {
            this.Sexes = ConvertToSelectList(typeof(Sex));
            this.Nationalities = ConvertToSelectList(typeof(Nationality));
        }

        public SelectList Nationalities { get; private set; }

        public SelectList Sexes { get; private set; }

        private static SelectList ConvertToSelectList(Type t)
        {
            var enumElements = Enum.GetValues(t).Cast<Enum>();

            var listItems = enumElements.Select(el => new { Id = el.ToString(), Name = el.ToString() })
                .Concat(new[] { new { Id = String.Empty, Name = "Any" } });

            return new SelectList(listItems, "Id", "Name", String.Empty);
        }
    }

}