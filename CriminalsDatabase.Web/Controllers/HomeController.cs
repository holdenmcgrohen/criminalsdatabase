﻿using CriminalsDatabase.Tasks.Commands;
using CriminalsDatabase.Web.Models;
using System;
using System.Web.Mvc;

namespace CriminalsDatabase.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
                return RedirectToAction("Search");
            else
                return RedirectToAction("Login");
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(UserLoginCommand command)
        {
            if (ModelState.IsValid)
            {
                var result = command.Process();
                if (result.Success)
                    return RedirectToAction("Search");
                else
                { 
                    ModelState.AddModelError(String.Empty, result.Error);
                    return Login();
                }
            }
            return Login();
        }

        [HttpGet]
        public ActionResult LogOut(UserLogOutCommand command)
        {
            var result = command.Process();
            if (result.Success)
                return RedirectToAction("Index");

            return Message("Error", result.Error);
        }

        [HttpGet]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(RegisterUserCommand command)
        {
            if (ModelState.IsValid)
            {
                var result = command.Process();
                if (result.Success)
                    return Message("Account created", "Now you can login using the credentials you specified");
                else
                {
                    ModelState.AddModelError(String.Empty, result.Error);
                    return Register();
                }
            }
            return Register();
        }

        public ActionResult Message(string header, string text)
        {
            return View("Message", new MessageViewModel { Header = header, Text = text });
        }

        [Authorize]
        public ActionResult Search()
        {
            return View(new SearchViewModel());
        }

        [HttpPost]
        [Authorize]
        public ActionResult Search(CreateSearchRequestCommand command)
        {
            command.CurrentUserLogin = User.Identity.Name;

            if (ModelState.IsValid)
            {
                var result = command.Process();
                if (result.Success)
                    return Message("Request accepted", "Once processed, the results will be emailed to you at the email address you provided");
                else
                {
                    ModelState.AddModelError(String.Empty, result.Error);
                    return Search();
                }
            }
            return Search();
        }
    }
}