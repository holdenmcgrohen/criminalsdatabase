﻿using CriminalsDatabase.Domain.Contracts;
using CriminalsDatabase.Infrastructure;
using CriminalsDatabase.Infrastructure.Configuration;
using System;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Web;

namespace CriminalsDatabase.Wcf
{
    class Program
    {
        static void Main(string[] args)
        {
            var config = ServiceLocator.GetInstance<WcfServiceConfig>();

            var endpoint = new Uri(config.Address);

            var host = new WebServiceHost(typeof(WcfService), endpoint);

            try
            {
                Console.WriteLine("Starting WCF web service at " + endpoint.ToString());

                ServiceEndpoint ep = host.AddServiceEndpoint(typeof(IWcfServiceContract), new WebHttpBinding(), "");
                host.Open();

                Console.WriteLine("Service started. Press <ENTER> to terminate");
                Console.ReadLine();

                host.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("An exception occurred: {0}", e.Message);
                host.Abort();
            }
        }
    }
}
