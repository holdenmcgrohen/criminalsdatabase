﻿using CriminalsDatabase.Domain.Contracts;
using CriminalsDatabase.Tasks.Commands;

namespace CriminalsDatabase.Wcf
{
    public class WcfService : IWcfServiceContract
    {
        public bool ProcessSearchRequest(int requestId)
        {
            var command = new ProcessSearchRequestCommand {  RequestId = requestId };
            var result = command.Process();
            return result.Success;
        }
    }
}
