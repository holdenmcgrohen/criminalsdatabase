﻿
IF (EXISTS (SELECT name  FROM master.dbo.sysdatabases  WHERE ('[' + name + ']' = 'CriminalsDatabase' OR name = 'CriminalsDatabase')))
BEGIN
	ALTER DATABASE [CriminalsDatabase] SET SINGLE_USER WITH ROLLBACK IMMEDIATE
	DROP DATABASE [CriminalsDatabase]
END

CREATE DATABASE [CriminalsDatabase] 
GO

USE [CriminalsDatabase]

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Users]') AND type in (N'U'))
	DROP TABLE [dbo].[Users]
GO

CREATE TABLE [dbo].[Users](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[Login] [nvarchar](50) NULL,
	[PasswordHash] [nvarchar](65) NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SearchRequests]') AND type in (N'U'))
	DROP TABLE [dbo].[SearchRequests]
GO

CREATE TABLE [dbo].[SearchRequests](
	[SearchRequestId] [int] IDENTITY(1,1) NOT NULL,
	[RequestedByUserId] [int] NOT NULL,
	[Email] [nvarchar](255) NOT NULL,
	[MaxResults] [int] NOT NULL,
	[Name] [nvarchar](255) NULL,
	[Sex] [int] NULL,
	[Nationality] [int] NULL,
	[AgeFrom] [int] NULL,
	[AgeTo] [int] NULL,
	[HeightFrom] [decimal](18, 5) NULL,
	[HeightTo] [decimal](18, 5) NULL,
	[WeightFrom] [decimal](18, 5) NULL,
	[WeightTo] [decimal](18, 5) NULL,
 CONSTRAINT [PK_SearchRequests] PRIMARY KEY CLUSTERED 
(
	[SearchRequestId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[SearchRequests]  WITH CHECK ADD  CONSTRAINT [FK_SearchRequests_Users] FOREIGN KEY([RequestedByUserId])
REFERENCES [dbo].[Users] ([UserId])
GO

ALTER TABLE [dbo].[SearchRequests] CHECK CONSTRAINT [FK_SearchRequests_Users]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CriminalProfiles]') AND type in (N'U'))
	DROP TABLE [dbo].[CriminalProfiles]
GO

CREATE TABLE [dbo].[CriminalProfiles](
	[CriminalProfileId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Sex] [int] NOT NULL,
	[Nationality] [int] NOT NULL,
	[Height] [decimal](18, 5) NOT NULL,
	[Weight] [decimal](18, 5) NOT NULL,
	[BirthDate] [datetime] NOT NULL,
 CONSTRAINT [PK_CriminalProfiles] PRIMARY KEY CLUSTERED 
(
	[CriminalProfileId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

INSERT INTO
	[dbo].[CriminalProfiles] ([Name], [Sex], [Nationality], [Height], [Weight], [BirthDate])
VALUES
	('Jane Doe 1', 1, 1, 172, 53.1, '2000-03-01'),
	('John Doe 2', 0, 2, 174, 56.2, '2000-05-01'),
	('Jane Doe 3', 1, 3, 176, 59.3, '2000-07-01'),
	('John Doe 4', 0, 4, 178, 62.4, '2000-09-01'),
	('Jane Doe 5', 1, 5, 180, 65.5, '2000-11-01'),
	('John Doe 6', 0, 0, 182, 68.6, '2001-01-01'),
	('Jane Doe 7', 1, 1, 184, 71.7, '2001-03-01'),
	('John Doe 8', 0, 2, 186, 74.8, '2001-05-01'),
	('Jane Doe 9', 1, 3, 188, 77.9, '2001-07-01'),
	('John Doe 10', 0, 4, 190, 81.0, '2001-09-01'),
	('Jane Doe 11', 1, 5, 192, 84.1, '2001-11-01'),
	('John Doe 12', 0, 0, 194, 87.2, '2002-01-01'),
	('Jane Doe 13', 1, 1, 196, 90.3, '2002-03-01'),
	('John Doe 14', 0, 2, 198, 93.4, '2002-05-01'),
	('Jane Doe 15', 1, 3, 200, 96.5, '2002-07-01'),
	('John Doe 16', 0, 4, 202, 99.6, '2002-09-01'),
	('Jane Doe 17', 1, 5, 204, 102.7, '2002-11-01'),
	('John Doe 18', 0, 0, 206, 105.8, '2003-01-01'),
	('Jane Doe 19', 1, 1, 208, 108.9, '2003-03-01'),
	('John Doe 20', 0, 2, 210, 112.0, '2003-05-01')
GO
