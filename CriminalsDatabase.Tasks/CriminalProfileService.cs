﻿using CriminalsDatabase.Domain;
using CriminalsDatabase.Infrastructure;
using System.Collections.Generic;
using System.Linq;
using System;

namespace CriminalsDatabase.Tasks
{
    public class CriminalProfileService
    {
        private SearchRequest _request;

        public CriminalProfileService(SearchRequest request)
        {
            this._request = request;
        }

        public void ProcessSearchRequest()
        {
            try
            {
                var profiles = this.FetchProfiles();
                var pdfFiles = this.CreatePdfFiles(profiles);
                this.EmailFiles(pdfFiles);
            }
            catch (Exception e)
            {
                Console.WriteLine("Failed to process request #{0}: {1}".F(_request.SearchRequestId, e.Message));
            }
        }

        private List<CriminalProfile> FetchProfiles()
        {
            using (var repository = new Repository())
            {
                var query = repository.CriminalProfiles.AsQueryable();

                if (_request.Name != null)
                    query = query.Where(p => p.Name.Contains(_request.Name));

                if (_request.Sex.HasValue)
                    query = query.Where(p => p.Sex == _request.Sex.Value);

                if (_request.Nationality.HasValue)
                    query = query.Where(p => p.Nationality == _request.Nationality.Value);

                if (_request.AgeFrom.HasValue)
                {
                    var maxBirthDate = DateTime.Now.AddYears(-_request.AgeFrom.Value);
                    query = query.Where(p => p.BirthDate <= maxBirthDate);
                }

                if (_request.AgeTo.HasValue)
                {
                    var minBirthDate = DateTime.Now.AddYears(-_request.AgeTo.Value);
                    query = query.Where(p => p.BirthDate >= minBirthDate);
                }

                if (_request.HeightFrom.HasValue)
                    query = query.Where(p => p.Height >= _request.HeightFrom.Value);

                if (_request.HeightTo.HasValue)
                    query = query.Where(p => p.Height <= _request.HeightTo.Value);

                if (_request.WeightFrom.HasValue)
                    query = query.Where(p => p.Weight >= _request.WeightFrom.Value);

                if (_request.WeightTo.HasValue)
                    query = query.Where(p => p.Weight <= _request.WeightTo.Value);

                return query.Take(_request.MaxResults).ToList();
            }
        }

        private IEnumerable<byte[]> CreatePdfFiles(List<CriminalProfile> profiles)
        {
            foreach (var profile in profiles)
            {
                var html = TemplateService.ProcessFromFile(@"CriminalProfile.cshtml", new CriminalProfileWrapper(profile));
                var pdf = PdfService.GeneratePdfFromHtml(html);
                yield return pdf;
            }
        }

        private void EmailFiles(IEnumerable<byte[]> pdfFiles)
        {
            var emailService = ServiceLocator.GetInstance<EmailService>();
            string subject = "Criminals database search results #" + _request.SearchRequestId;

            if (!pdfFiles.Any())
                emailService.SendMail(_request.Email, subject, "No profiles met your search criteria.");

            string body = "Attached are criminal profiles that met your search criteria.";
            int fileIndex = 0;

            foreach (var batch in pdfFiles.SplitIntoBatches(10))
            {
                var filesToAttach = batch.ToDictionary(fileBytes => "Profile_{0}.pdf".F(++fileIndex), fileBytes => fileBytes);
                emailService.SendMail(_request.Email, subject, body, filesToAttach);
            }
        }

        public class CriminalProfileWrapper
        {
            public CriminalProfileWrapper(CriminalProfile profile)
            {
                this.Name = profile.Name;
                this.Age = (DateTime.Now - profile.BirthDate).TotalYears().ToString();
                this.Nationality = profile.Nationality.ToString();
                this.Sex = profile.Sex.ToString();
                this.Weight = profile.Weight.ToString(@"#,0.00");
                this.Height = profile.Height.ToString(@"#,0.00");
            }

            public string Age { get; private set; }
            public string Height { get; private set; }
            public string Name { get; private set; }
            public string Nationality { get; private set; }
            public string Sex { get; private set; }
            public string Weight { get; private set; }
        }
    }
}
