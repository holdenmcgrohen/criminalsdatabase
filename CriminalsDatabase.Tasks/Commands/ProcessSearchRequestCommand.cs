﻿using CriminalsDatabase.Infrastructure;
using CriminalsDatabase.Infrastructure.Commands;
using System;
using System.Linq;
using System.Threading;

namespace CriminalsDatabase.Tasks.Commands
{
    public class ProcessSearchRequestCommand : CommandBase
    {
        public int RequestId { get; set; }

        protected override CommandResult ProcessInternal()
        {
            using (var repository = new Repository())
            {
                var request = repository.SearchRequests.Where(r => r.SearchRequestId == this.RequestId).SingleOrDefault();
                if (request == null)
                    return CommandResult.Failed("Incorrect request ID");

                Async(() => { new CriminalProfileService(request).ProcessSearchRequest(); });

                return CommandResult.Ok();
            }
        }

        private static void Async(Action action)
        {
            if (action == null) throw new ArgumentNullException("action");

            var currentCulture = Thread.CurrentThread.CurrentCulture;
            var currentUiCulture = Thread.CurrentThread.CurrentUICulture;

            var callBack = new WaitCallback(delegate
            {
                Thread.CurrentThread.CurrentCulture = currentCulture;
                Thread.CurrentThread.CurrentUICulture = currentUiCulture;
                action();
            });

            ThreadPool.QueueUserWorkItem(callBack);
        }
    }
}
