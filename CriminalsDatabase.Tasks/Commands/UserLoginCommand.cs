﻿using CriminalsDatabase.Infrastructure;
using CriminalsDatabase.Infrastructure.Commands;
using CriminalsDatabase.Infrastructure.Helpers;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Security;

namespace CriminalsDatabase.Tasks.Commands
{
    public class UserLoginCommand : CommandBase
    {
        [Required(ErrorMessage ="Login not specified")]
        public string Login { get; set; }

        [Required(ErrorMessage = "Password not specified")]
        public string Password { get; set; }

        protected override CommandResult ProcessInternal()
        {
            var hash = Hash.Calculate(this.Password);

            using (var repository = new Repository())
            {
                var user = repository.Users.Where(u => u.Login == this.Login && u.PasswordHash == hash).SingleOrDefault();
                if (user == null)
                    return CommandResult.Failed("Incorrect login or password");

                FormsAuthentication.SetAuthCookie(user.Login, true);

                return CommandResult.Ok();
            }
        }
    }
}
