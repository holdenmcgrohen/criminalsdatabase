﻿using CriminalsDatabase.Infrastructure.Commands;
using System.Web.Security;

namespace CriminalsDatabase.Tasks.Commands
{
    public class UserLogOutCommand : CommandBase
    {
        protected override CommandResult ProcessInternal()
        {
            FormsAuthentication.SignOut();

            return CommandResult.Ok();
        }
    }
}
