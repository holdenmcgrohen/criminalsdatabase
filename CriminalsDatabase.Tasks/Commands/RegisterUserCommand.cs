﻿using CriminalsDatabase.Domain;
using CriminalsDatabase.Infrastructure;
using CriminalsDatabase.Infrastructure.Commands;
using CriminalsDatabase.Infrastructure.Helpers;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace CriminalsDatabase.Tasks.Commands
{
    public class RegisterUserCommand : CommandBase
    {
        private const string _loginRegex = @"^[a-zA-Z0-9]{1,50}$";

        [Required(ErrorMessage = "Login not specified")]
        [RegularExpression(_loginRegex, ErrorMessage = "Invalid login")]
        public string Login { get; set; }

        [Required(ErrorMessage = "Password not specified")]
        public string Password { get; set; }

        protected override CommandResult ProcessInternal()
        {
            using (var repository = new Repository())
            {
                var loginTaken = repository.Users.Any(u => u.Login == this.Login);
                if (loginTaken)
                    return CommandResult.Failed("This login is already taken");

                repository.Users.Add(new User { Login = this.Login, PasswordHash = Hash.Calculate(this.Password) });
                repository.SaveChanges();

                return CommandResult.Ok();
            }
        }
    }
}
