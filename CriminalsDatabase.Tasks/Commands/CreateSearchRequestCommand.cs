﻿using CriminalsDatabase.Domain;
using CriminalsDatabase.Domain.Contracts;
using CriminalsDatabase.Infrastructure;
using CriminalsDatabase.Infrastructure.Commands;
using CriminalsDatabase.Infrastructure.Configuration;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;

namespace CriminalsDatabase.Tasks.Commands
{
    public class CreateSearchRequestCommand : CommandBase
    {
        private const string _emailRegex = @"^(([^<>()[\]\\.,;:\s@\""]+(\.[^<>()[\]\\.,;:\s@\""]+)*)|(\"".+\""))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z\-0-9]{2,}))$";

        public string CurrentUserLogin { get; set; }

        [Required(ErrorMessage = "Email not specified")]
        [RegularExpression(_emailRegex, ErrorMessage = "Invalid email address")]
        public string Email { get; set; }        

        [Required(ErrorMessage = "Number of results not specified")]
        [Range(1, 999, ErrorMessage = "Number of enries must be between 1 and 999")]
        public int? MaxResults { get; set; }

        [StringLength(255, ErrorMessage = "Name cannot be longer than 255 characters")]
        public string Name { get; set; }

        public Sex? Sex { get; set; }

        public Nationality? Nationality { get; set; }

        [Range(1, 99, ErrorMessage = "Age must be between 1 and 999")]
        public int? AgeFrom { get; set; }

        [Range(1, 99, ErrorMessage = "Age must be between 1 and 999")]
        public int? AgeTo { get; set; }

        [Range(1, 99, ErrorMessage = "Height must be between 1 and 250")]
        public decimal? HeightFrom { get; set; }

        [Range(1, 99, ErrorMessage = "Height must be between 1 and 250")]
        public decimal? HeightTo { get; set; }

        [Range(1, 99, ErrorMessage = "Weight must be between 1 and 250")]
        public decimal? WeightFrom { get; set; }

        [Range(1, 99, ErrorMessage = "Weight must be between 1 and 250")]
        public decimal? WeightTo { get; set; }

        protected override CommandResult ProcessInternal()
        {
            using (var repository = new Repository())
            {
                var user = repository.Users.Where(u => u.Login == this.CurrentUserLogin).SingleOrDefault();
                if (user == null)
                    return CommandResult.Failed("Invalid credentials supplied");

                if(this.AgeFrom.HasValue && this.AgeTo.HasValue && this.AgeFrom.Value > this.AgeTo.Value)
                    return CommandResult.Failed("Minimum age should be smaller than maximum age");

                if (this.HeightFrom.HasValue && this.HeightTo.HasValue && this.HeightFrom.Value > this.HeightTo.Value)
                    return CommandResult.Failed("Minimum height should be smaller than maximum Height");

                if (this.WeightFrom.HasValue && this.WeightTo.HasValue && this.WeightFrom.Value > this.WeightTo.Value)
                    return CommandResult.Failed("Minimum weight should be smaller than maximum Weight");

                var request = new SearchRequest
                {
                    RequestedBy = user,
                    Email = this.Email,
                    MaxResults = this.MaxResults.Value,
                    Name = this.Name,
                    Nationality = this.Nationality,
                    Sex = this.Sex,
                    AgeFrom = this.AgeFrom,
                    AgeTo = this.AgeTo,
                    HeightFrom = this.HeightFrom,
                    HeightTo = this.HeightTo,
                    WeightFrom = this.WeightFrom,
                    WeightTo = this.WeightTo
                };

                repository.SearchRequests.Add(request);
                repository.SaveChanges();

                try
                {
                    CallService(request.SearchRequestId);
                    return CommandResult.Ok();
                }
                catch(Exception e)
                {
                    return CommandResult.Failed("Failed to call service: " + e.Message);
                }
            }
        }

        private static bool CallService(int requestId)
        {
            var serviceConfig = ServiceLocator.GetInstance<WcfServiceConfig>();

            using (var channelFactory = new ChannelFactory<IWcfServiceContract>(new WebHttpBinding(), serviceConfig.Address))
            {
                channelFactory.Endpoint.Behaviors.Add(new WebHttpBehavior());

                var channel = channelFactory.CreateChannel();

                return channel.ProcessSearchRequest(requestId);
            }
        }
    }
}
