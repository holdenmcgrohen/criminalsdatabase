﻿using System.ServiceModel;
using System.ServiceModel.Web;

namespace CriminalsDatabase.Domain.Contracts
{
    [ServiceContract]
    public interface IWcfServiceContract
    {
        [OperationContract]
        [WebGet]
        bool ProcessSearchRequest(int requestId);
    }
}
