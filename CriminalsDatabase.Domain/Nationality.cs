﻿namespace CriminalsDatabase.Domain
{
    public enum Nationality
    {
        American = 0,
        British = 1,
        French = 2,
        German = 3,
        Russian = 4,
        Armenian = 5
    }
}
