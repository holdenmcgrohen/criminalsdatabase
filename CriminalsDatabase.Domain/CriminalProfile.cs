﻿using System;

namespace CriminalsDatabase.Domain
{
    public class CriminalProfile
    {
        public int CriminalProfileId { get; set; }

        public string Name { get; set; }

        public Sex Sex { get; set; }

        public Nationality Nationality { get; set; }

        public DateTime BirthDate { get; set; }

        public decimal Height { get; set; }

        public decimal Weight { get; set; }
    }
}
