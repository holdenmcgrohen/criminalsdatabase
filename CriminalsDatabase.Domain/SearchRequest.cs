﻿namespace CriminalsDatabase.Domain
{
    public class SearchRequest
    {
        public int SearchRequestId { get; set; }

        public int RequestedByUserId { get; set; }
        
        public User RequestedBy { get; set; }

        public string Email { get; set; }        

        public int MaxResults { get; set; }

        public string Name { get; set; }

        public Sex? Sex { get; set; }

        public Nationality? Nationality { get; set; }

        public int? AgeFrom { get; set; }

        public int? AgeTo { get; set; }

        public decimal? HeightFrom { get; set; }

        public decimal? HeightTo { get; set; }

        public decimal? WeightFrom { get; set; }

        public decimal? WeightTo { get; set; }
    }
}
