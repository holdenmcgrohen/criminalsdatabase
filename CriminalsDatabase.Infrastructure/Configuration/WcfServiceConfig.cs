﻿namespace CriminalsDatabase.Infrastructure.Configuration
{
    public class WcfServiceConfig
    {
        public string Address { get; set; }
    }
}
