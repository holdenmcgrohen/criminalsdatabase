﻿namespace CriminalsDatabase.Infrastructure.Configuration
{
    public class RepositoryConfig
    {
        public string ConnectionString { get; set; }
    }
}
