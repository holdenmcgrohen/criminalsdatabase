﻿using Castle.Windsor;
using Castle.Windsor.Configuration.Interpreters;

namespace CriminalsDatabase.Infrastructure
{
    public class ServiceLocator
    {
        private static WindsorContainer _container = new WindsorContainer(new XmlInterpreter());

        public static T GetInstance<T>()
        {
            return _container.Resolve<T>();
        }
    }
}
