﻿using CriminalsDatabase.Domain;
using CriminalsDatabase.Infrastructure.Configuration;
using System.Data.Entity;

namespace CriminalsDatabase.Infrastructure
{
    public class Repository : DbContext
    {
        private static string _connectionString;

        static Repository()
        {
            var repositoryConfig = ServiceLocator.GetInstance<RepositoryConfig>();
            _connectionString = repositoryConfig.ConnectionString;
        }

        public Repository() 
            : base(_connectionString)
        {
        }

        public DbSet<User> Users { get; set; }

        public DbSet<SearchRequest> SearchRequests { get; set; }

        public DbSet<CriminalProfile> CriminalProfiles { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<SearchRequest>()
                .HasRequired(e => e.RequestedBy)
                .WithMany()
                .HasForeignKey(e => e.RequestedByUserId);
        }
    }
}
