﻿using RazorEngine;
using RazorEngine.Configuration;
using RazorEngine.Templating;
using System;
using System.IO;
using System.Web;

namespace CriminalsDatabase.Infrastructure
{
    public class TemplateService
    {
        static TemplateService()
        {
            var config = new TemplateServiceConfiguration
            {
                BaseTemplateType = typeof(HtmlTemplateBase<>)
            };

            var service = new RazorEngine.Templating.TemplateService(config);
            Razor.SetTemplateService(service);
        }

        private static string _templatesRootPath = @"Content\Templates\";

        public static string ProcessFromFile<TModel>(string templateFileName, TModel model)
        {
            if (string.IsNullOrEmpty(templateFileName))
                throw new ArgumentNullException("templateFileName");

            if (model == null)
                throw new ArgumentNullException("model");

            return Process(LoadFromFile(templateFileName), model);
        }

        private static string Process<TModel>(string template, TModel model)
        {
            if (string.IsNullOrEmpty(template))
                throw new ArgumentNullException("template");

            if (model == null)
                throw new ArgumentNullException("model");

            try
            {
                return Razor.Parse(template, model);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error processing template", ex);
            }
        }

        private static string LoadFromFile(string fileName)
        {
            var fullPath = _templatesRootPath + fileName;
            fullPath = HttpContext.Current != null
                           ? HttpContext.Current.Server.MapPath(fullPath)
                           : fullPath.Replace("/", "\\");
            return File.ReadAllText(fullPath, System.Text.Encoding.UTF8);
        }
    }

}
