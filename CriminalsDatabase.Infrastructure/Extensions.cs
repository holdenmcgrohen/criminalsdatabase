﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CriminalsDatabase.Infrastructure
{
    public static class Extensions
    {
        public static string F(this string st, params object[] args)
        {
            return string.Format(st, args);
        }

        public static int TotalYears(this TimeSpan span)
        {
            var zeroTime = new DateTime(1, 1, 1);
            return (zeroTime + span).Year - 1;
        }

        public static IEnumerable<IEnumerable<T>> SplitIntoBatches<T>(this IEnumerable<T> source, int batchSize)
        {
            using (var enumerator = source.GetEnumerator())
                while (enumerator.MoveNext())
                    yield return YieldBatchElements(enumerator, batchSize - 1);
        }

        private static IEnumerable<T> YieldBatchElements<T>(IEnumerator<T> source, int batchSize)
        {
            yield return source.Current;
            for (int i = 0; i < batchSize && source.MoveNext(); i++)
                yield return source.Current;
        }
    }
}
