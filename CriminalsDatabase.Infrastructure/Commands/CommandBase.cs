﻿using System;

namespace CriminalsDatabase.Infrastructure.Commands
{
    public abstract class CommandBase
    {
        public CommandResult Process()
        {
            try
            {
                return this.ProcessInternal();
            }
            catch (Exception e)
            {
                return CommandResult.Failed(e.ToString());
            }
        }

        protected abstract CommandResult ProcessInternal();
    }
}
