﻿namespace CriminalsDatabase.Infrastructure.Commands
{
    public class CommandResult
    {
        public bool Success { get; set; }

        public string Error { get; set; }

        public static CommandResult Ok()
        {
            return new CommandResult { Success = true };
        }

        public static CommandResult Failed(string eror)
        {
            return new CommandResult { Success = false, Error = eror };
        }
    }
}
