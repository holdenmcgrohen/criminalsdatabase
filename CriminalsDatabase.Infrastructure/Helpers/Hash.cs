﻿using System.Security.Cryptography;
using System.Text;

namespace CriminalsDatabase.Infrastructure.Helpers
{
    public static class Hash
    {
        public static string Calculate(string text)
        {
            var hashBytes = (new SHA256Managed()).ComputeHash(Encoding.UTF8.GetBytes(text));

            var sBuilder = new StringBuilder();
            for (var i = 0; i < hashBytes.Length; i++)
                sBuilder.Append(hashBytes[i].ToString("x2"));
            return sBuilder.ToString();
        }        
    }
}
