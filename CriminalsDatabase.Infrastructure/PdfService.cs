﻿using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System;
using System.IO;

namespace CriminalsDatabase.Infrastructure
{
    public static class PdfService
    {
        public static byte[] GeneratePdfFromHtml(string html)
        {
            using (var stream = new MemoryStream())
            {
                TextReader reader = new StringReader(html);

                var document = new Document(PageSize.A4);

                PdfWriter.GetInstance(document, stream);

                var worker = new HTMLWorker(document);

                FontFactory.Register(Environment.ExpandEnvironmentVariables(@"%systemroot%\fonts\Arial.TTF"));
                var style = new StyleSheet();
                style.LoadTagStyle("body", "face", "Arial");
                style.LoadTagStyle("body", "encoding", BaseFont.IDENTITY_H);
                worker.Style = style;

                document.Open();
                worker.StartDocument();

                worker.Parse(reader);

                worker.EndDocument();
                worker.Close();
                document.Close();

                return stream.ToArray();
            }
        }
    }
}
