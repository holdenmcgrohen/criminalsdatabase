﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;

namespace CriminalsDatabase.Infrastructure
{
    public class EmailService
    {
        public string Host { get; set; }
        public int Port { get; set; }
        public bool UseSSL { get; set; }
        public int Timeout { get; set; }
        public bool UseDefaultCredentials { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string DefaultFromAddress { get; set; }

        public void SendMail(string recipient, string subject, string body, Dictionary<string, byte[]> attachments = null)
        {
            var msg = new MailMessage(this.DefaultFromAddress, recipient, subject, body);

            if (attachments != null)
                foreach (var attachment in attachments)
                {
                    var ct = new ContentType(MediaTypeNames.Application.Octet) { Name = attachment.Key };
                    msg.Attachments.Add(new Attachment(new MemoryStream(attachment.Value), ct));
                }

            var smtpClient = this.ConfigureSmtp();
            smtpClient.Send(msg);
        }

        private SmtpClient ConfigureSmtp()
        {
            var smtp = new SmtpClient();

            if (!String.IsNullOrEmpty(Host))
                smtp.Host = this.Host;

            if (Port != 0)
                smtp.Port = this.Port;

            smtp.EnableSsl = this.UseSSL;

            if (Timeout != 0)
                smtp.Timeout = this.Timeout;

            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;

            if (!UseDefaultCredentials)
            {
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential(this.UserName, this.Password);
            }

            return smtp;
        }
    }
}
